package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ProjectsEndpoint {

    @Nullable
    @WebMethod
    @PostMapping("/add")
    List<Project> add(@WebParam(name = "projects") @RequestBody List<Project> projects);

    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    List<Project> findAll();

    @WebMethod
    @DeleteMapping("/remove")
    void remove(@WebParam(name = "projects") @RequestBody List<Project> projects);

    @WebMethod
    @DeleteMapping("/removeAll")
    void removeAll();

    @Nullable
    @WebMethod
    @PostMapping("/save")
    List<Project> save(@WebParam(name = "projects") @RequestBody List<Project> projects);

}
