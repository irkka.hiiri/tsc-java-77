package ru.tsc.ichaplygina.taskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.service.ProjectService;
import ru.tsc.ichaplygina.taskmanager.service.TaskService;
import ru.tsc.ichaplygina.taskmanager.util.UserUtil;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/task/create")
    public String create() {
        taskService.write(UserUtil.getUserId(), new Task("New Task " + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskService.removeById(UserUtil.getUserId(), id);
        return "redirect:/tasks";
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute("task") Task task, BindingResult result) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.write(UserUtil.getUserId(), task);
        return "redirect:/tasks";
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Task task = taskService.findById(UserUtil.getUserId(), id);
        return new ModelAndView("task-edit", "task", task);
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @ModelAttribute("projects")
    public Collection<Project> getProjects() {
        return projectService.findAll(UserUtil.getUserId());
    }

}
