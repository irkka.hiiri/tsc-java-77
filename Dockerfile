FROM tomcat:9.0.50-jdk8-adoptopenjdk-openj9
RUN rm -rf /usr/local/tomcat/webapps/*
COPY ./target/taskmanager.war /usr/local/tomcat/webapps/ROOT.war
CMD ["catalina.sh","run"]
